<?php

namespace BetaMFD\PayrollBundle\Utils;

use Doctrine\ORM\Mapping as ORM;

class WageConverter
{
    // constants set as strings for bcmath instead of floats
    // Most of the floats repeat forever,
    // so I kill them after 40 places which is WAY more than I need
    //
    // Storing the values will reduce the overall calculations required. =)

    #######################
    #      Months per     #
    #######################
    const MONTHS_PER_YR = '12';
    const MONTHS_PER_SEMIMONTH = '.5'; //12/24
    // 12 months / 26 biweeks in a year
    const MONTHS_PER_BIWEEK = '.4615384615384615384615384615384615384615';
    // 12 months / 52 weeks in a year
    const MONTHS_PER_WEEK = '.2307692307692307692307692307692307692308';
    // 12 months / 260 days in a year
    const MONTHS_PER_DAY = '.0461538461538461538461538461538461538461';
    // 12 months / 2080 hours
    const MONTHS_PER_HOUR = '0.0057692307692307692307692307692307692308';

    #######################
    #    Semimonths per   #
    #######################
    const SEMIMONTHS_PER_YR = '24';
    const SEMIMONTHS_PER_MONTH = '2'; // 24 / 12
    // 24 semi months / 26 biweeks in a year
    const SEMIMONTHS_PER_BIWEEK = '0.9230769230769230769230769230769230769230';
    // 24 semi months / 52 weeks in a year (also 12 / 26)
    const SEMIMONTHS_PER_WEEK = '.4615384615384615384615384615384615384615';
    // 24 semi months / 260 days in a year
    const SEMIMONTHS_PER_DAY = '0.0923076923076923076923076923076923076923';
    // 24 semi months / 2080 hours
    const SEMIMONTHS_PER_HOUR = '0.0115384615384615384615384615384615384615';

    ######################
    #     Biweeks per    #
    ######################
    const BIWEEKS_PER_YR = '26';
    // 26 biweeks per year / 12 months in a year
    const BIWEEKS_PER_MONTH = '2.1666666666666666666666666666666666666667';
    // 26 biweeks per year / 24 semimonths in a year
    const BIWEEKS_PER_SEMIMONTH = '1.0833333333333333333333333333333333333333';
    // 26 biweeks per year / 52 weeks in a year
    const BIWEEKS_PER_WEEK = '.5';
    // 26 biweeks per year / 260 days in a year
    const BIWEEKS_PER_DAY = '.1';
    // 1 biweek / 80 hrs/biweek
    const BIWEEKS_PER_HOUR = '.0125';

    ######################
    #      Weeks per     #
    ######################
    const WEEKS_PER_YR = '52';
    // 52 weeks per year / 12 months in a year
    const WEEKS_PER_MONTH = '4.333333333333333333333333333333333333333';
    // 52 weeks per year / 24 semimonths in a year
    const WEEKS_PER_SEMIMONTH = '2.1666666666666666666666666666666666666667';
    // 52 weeks per year / 26 biweeks in a year
    const WEEKS_PER_BIWEEK = '2';
    // 52 weeks per year / 260 days in a year
    const WEEKS_PER_DAY = '.2';
    // 1 wk/ 40hrs/wk
    const WEEKS_PER_HOUR = '.025';

    #######################
    #       Days per      #
    #######################
    const DAYS_PER_YR = 260; //5 days per week * 52 weeks
    // 5 days per week * 52 weeks / 12 months
    // or 260 days per year / 12 months
    const DAYS_PER_MONTH = '21.6666666666666666666666666666666666666667';
    // 260 days per year / 24 semimonths per year
    const DAYS_PER_SEMIMONTH = '10.8333333333333333333333333333333333333333';
    const DAYS_PER_BIWEEK = '10'; //2 weeks
    const DAYS_PER_WEEK = '5'; //M-F, standard work week
    // 8 hours per day / 1 day (or 260 days/yr / 2080 hrs/yr)
    const DAYS_PER_HOUR = '.125';

    #######################
    #       Hours per     #
    #######################
    const HOURS_PER_YR = '2080'; //52 wks * 40hrs
    // 2080 hours in a year / 12 months
    const HOURS_PER_MONTH = '173.3333333333333333333333333333333333333333';
    // 2080 hours in a year / 24 semimonths
    const HOURS_PER_SEMIMONTH = '86.6666666666666666666666666666666666666667';
    const HOURS_PER_BIWEEK = '80'; //two weeks, 8*10
    const HOURS_PER_WEEK = '40'; //standard work week, 8*5
    const HOURS_PER_DAY = '8'; //standard 8 hour day


    ###########################################################################
    #                             Annual to . . .                             #
    ###########################################################################

    static public function annualToMonthly($annual, $decimals = 6)
    {
        return bcdiv($annual, self::MONTHS_PER_YR, $decimals);
    }
    static public function annualToSemimonthly($annual, $decimals = 6)
    {
        return bcdiv($annual, self::SEMIMONTHS_PER_YR, $decimals);
    }
    static public function annualToBiweekly($annual, $decimals = 6)
    {
        return bcdiv($annual, self::BIWEEKS_PER_YR, $decimals);
    }
    static public function annualToWeekly($annual, $decimals = 6)
    {
        return bcdiv($annual, self::WEEKS_PER_YR, $decimals);
    }
    static public function annualToDaily($annual, $decimals = 6)
    {
        return bcdiv($annual, self::DAYS_PER_YR, $decimals);
    }
    static public function annualToHourly($annual, $decimals = 6)
    {
        return bcdiv($annual, self::HOURS_PER_YR, $decimals);
    }

    ###########################################################################
    #                            Monthly to . . .                             #
    ###########################################################################

    static public function monthlyToAnnual($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_YR, $decimals);
    }

    static public function monthlyToSemimonthly($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_SEMIMONTH, $decimals);
    }

    static public function monthlyToBiweekly($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_BIWEEK, $decimals);
    }

    static public function monthlyToWeekly($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_WEEK, $decimals);
    }

    static public function monthlyToDaily($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_DAY, $decimals);
    }

    static public function monthlyToHourly($monthly, $decimals = 6)
    {
        return bcmul($monthly, self::MONTHS_PER_HOUR, $decimals);
    }
    ###########################################################################
    #                          Semimonthly to . . .                           #
    ###########################################################################

    static public function semimonthlyToAnnual($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_YR, $decimals);
    }

    static public function semimonthlyToMonthly($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_MONTH, $decimals);
    }

    static public function semimonthlyToBiweekly($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_BIWEEK, $decimals);
    }

    static public function semimonthlyToWeekly($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_WEEK, $decimals);
    }

    static public function semimonthlyToDaily($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_DAY, $decimals);
    }

    static public function semimonthlyToHourly($semimonthly, $decimals = 6)
    {
        return bcmul($semimonthly, self::SEMIMONTHS_PER_HOUR, $decimals);
    }

    ###########################################################################
    #                            Biweekly to . . .                            #
    ###########################################################################

    static public function biweeklyToAnnual($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_YR, $decimals);
    }

    static public function biweeklyToMonthly($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_MONTH, $decimals);
    }

    static public function biweeklyToSemimonthly($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_SEMIMONTH, $decimals);
    }

    static public function biweeklyToWeekly($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_WEEK, $decimals);
    }

    static public function biweeklyToDaily($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_DAY, $decimals);
    }

    static public function biweeklyToHourly($biweekly, $decimals = 6)
    {
        return bcmul($biweekly, self::BIWEEKS_PER_HOUR, $decimals);
    }

    ###########################################################################
    #                             Weekly to . . .                             #
    ###########################################################################

    static public function weeklyToAnnual($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_YR, $decimals);
    }

    static public function weeklyToMonthly($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_MONTH, $decimals);
    }

    static public function weeklyToSemimonthly($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_SEMIMONTH, $decimals);
    }

    static public function weeklyToBiweekly($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_BIWEEK, $decimals);
    }

    static public function weeklyToDaily($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_DAY, $decimals);
    }

    static public function weeklyToHourly($weekly, $decimals = 6)
    {
        return bcmul($weekly, self::WEEKS_PER_HOUR, $decimals);
    }

    ###########################################################################
    #                              Daily to . . .                             #
    ###########################################################################


    static public function dailyToAnnual($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_YR, $decimals);
    }

    static public function dailyToMonthly($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_MONTH, $decimals);
    }

    static public function dailyToSemimonthly($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_SEMIMONTH, $decimals);
    }

    static public function dailyToBiweekly($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_BIWEEK, $decimals);
    }

    static public function dailyToWeekly($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_WEEK, $decimals);
    }

    static public function dailyToHourly($daily, $decimals = 6)
    {
        return bcmul($daily, self::DAYS_PER_HOUR, $decimals);
    }

    ###########################################################################
    #                              Hourly to . . .                             #
    ###########################################################################


    static public function hourlyToAnnual($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_YR, $decimals);
    }

    static public function hourlyToMonthly($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_MONTH, $decimals);
    }

    static public function hourlyToSemimonthly($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_SEMIMONTH, $decimals);
    }

    static public function hourlyToBiweekly($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_BIWEEK, $decimals);
    }

    static public function hourlyToWeekly($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_WEEK, $decimals);
    }

    static public function hourlyToDaily($hourly, $decimals = 6)
    {
        return bcmul($hourly, self::HOURS_PER_DAY, $decimals);
    }


}
