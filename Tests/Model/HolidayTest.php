<?php

namespace BetaMFD\PayrollBundle\Tests\Utils;

use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

use DateTime;
class HolidayTest extends TestCase
{

    private $holiday;

    public function __construct()
    {
        parent::__construct();
        $this->holiday = $this->getMockForAbstractClass('\BetaMFD\PayrollBundle\Model\Holiday');
        //$this->ee = new class extends Employee {};
    }

    public function testGetDateOff()
    {
        $h = clone $this->holiday;
        $string = 'December 25';
        $h->setString($string);
        $this->assertEquals('2016-12-26', $h->getDateOff(2016)->format('Y-m-d'));
        $this->assertEquals('2011-12-26', $h->getDateOff(2011)->format('Y-m-d'));
        $this->assertEquals('2010-12-24', $h->getDateOff(2010)->format('Y-m-d'));

    }

    public function testGetDateOffToFri()
    {
        $h = clone $this->holiday;
        $string = 'December 25';
        $h->setString($string);
        $h->setWeekendMove('Fri');
        $this->assertEquals('2016-12-23', $h->getDateOff(2016)->format('Y-m-d'));
        $this->assertEquals('2011-12-23', $h->getDateOff(2011)->format('Y-m-d'));
        $this->assertEquals('2010-12-24', $h->getDateOff(2010)->format('Y-m-d'));

    }

    public function testGetDateOffToMon()
    {
        $h = clone $this->holiday;
        $h->setString('December 25');
        $h->setWeekendMove('Mon');
        $this->assertEquals('2016-12-26', $h->getDateOff(2016)->format('Y-m-d'));
        $this->assertEquals('2011-12-26', $h->getDateOff(2011)->format('Y-m-d'));
        $this->assertEquals('2010-12-27', $h->getDateOff(2010)->format('Y-m-d'));
    }

    public function testDateString()
    {
        $h = clone $this->holiday;
        $h->setString('December 25');
        for ($i = 2000; $i <= 2020; $i++) {
            $this->assertEquals("$i-12-25", $h->getDateString($i));
        }
    }

    public function testDateStringWithSetYear()
    {
        $h = clone $this->holiday;
        $h->setString('3/30/2018');
        for ($i = 2000; $i <= 2020; $i++) {
            $this->assertEquals("2018-03-30", $h->getDateString($i));
        }
    }

    public function testGetDate()
    {
        $h = clone $this->holiday;
        $h->setString('December 25');
        for ($i = 2000; $i <= 2020; $i++) {
            $this->assertEquals("$i-12-25", $h->getDate($i)->format('Y-m-d'));
        }
    }

    public function testGetDateWithSetYear()
    {
        $h = clone $this->holiday;
        $h->setString('3/30/2018');
        for ($i = 2000; $i <= 2020; $i++) {
            $this->assertEquals("2018-03-30", $h->getDate($i)->format('Y-m-d'));
        }
    }

}
