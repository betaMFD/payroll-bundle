<?php

namespace BetaMFD\PayrollBundle\Tests\Utils;

use BetaMFD\PayrollBundle\Model\Employee;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

use DateTime;
class EmployeeTest extends TestCase
{

    private $ee;

    public function __construct()
    {
        parent::__construct();
        $this->ee = $this->getMockForAbstractClass('\BetaMFD\PayrollBundle\Model\Employee');
        //$this->ee = new class extends Employee {};
    }

    public function testAge()
    {
        $this->assertNull($this->ee->getAge());
        $date = new DateTime();
        $date->modify('-265 months');
        $this->ee->setBirthDate($date);
        $this->assertEquals(22, $this->ee->getAge());
    }

    public function testName()
    {
        $this->assertNull($this->ee->getLastInitial());
        $this->assertNull($this->ee->getFirstInitial());
        $this->assertNull($this->ee->getInitials());
        $this->ee->setFirst('Tesitha');
        $this->ee->setLast('McTesterton');
        $this->assertEquals('M', $this->ee->getLastInitial());
        $this->assertEquals('T', $this->ee->getFirstInitial());
        $this->assertEquals('TM', $this->ee->getInitials());
        $this->assertEquals('Tesitha McTesterton', $this->ee->getName());
    }

    public function testVacationAccural()
    {
        $this->ee->setVacationAccrualRate(2);
        $this->assertEquals(2, $this->ee->getVacationAccrualRate());
        $this->assertTrue($this->ee->canAccrueVacation());
        $this->ee->setCanAccrueVacation(false);
        $this->assertNull($this->ee->getVacationAccrualRate());
        $this->assertFalse($this->ee->canAccrueVacation());
    }

    public function testVacationAdd()
    {
        $this->ee->addVacation('100.23');
        $this->assertEquals('100.23', $this->ee->getVacationBalance());
        $this->ee->addVacation('545.2513');
        $this->assertEquals('645.4813', $this->ee->getVacationBalance());
        $this->ee->addVacation('-12.4896');
        $this->assertEquals('632.9917', $this->ee->getVacationBalance());
    }

    public function testSalaryHourly()
    {
        $this->assertNull($this->ee->isSalary());
        $this->assertNull($this->ee->isHourly());
        $this->ee->setIsSalary(true);
        $this->assertTrue($this->ee->isSalary());
        $this->assertFalse($this->ee->isHourly());
        $this->ee->setIsSalary(false);
        $this->assertFalse($this->ee->isSalary());
        $this->assertTrue($this->ee->isHourly());
        $this->ee->setIsHourly(false);
        $this->assertTrue($this->ee->isSalary());
        $this->assertFalse($this->ee->isHourly());
        $this->ee->setIsHourly(true);
        $this->assertFalse($this->ee->isSalary());
        $this->assertTrue($this->ee->isHourly());
    }

    public function testFullTimePartTime()
    {
        $this->assertTrue($this->ee->isFullTime());
        $this->assertFalse($this->ee->isPartTime());
        $this->ee->setfullTime(true);
        $this->assertTrue($this->ee->isFullTime());
        $this->assertFalse($this->ee->isPartTime());
        $this->ee->setfullTime(false);
        $this->assertFalse($this->ee->isFullTime());
        $this->assertTrue($this->ee->isPartTime());
    }

    public function testStatus()
    {
        $this->assertTrue($this->ee->isActive());
        $this->assertFalse($this->ee->isOfficer());
        $this->assertTrue($this->ee->isInHouse());

        $this->ee->setActive(false);
        $this->ee->setOfficer(false);
        $this->ee->setInHouse(false);
        $this->assertFalse($this->ee->isActive());
        $this->assertFalse($this->ee->isOfficer());
        $this->assertFalse($this->ee->isInHouse());

        $this->ee->setActive(true);
        $this->ee->setOfficer(true);
        $this->ee->setInHouse(true);
        $this->assertTrue($this->ee->isActive());
        $this->assertTrue($this->ee->isOfficer());
        $this->assertTrue($this->ee->isInHouse());
    }

    public function testHours()
    {
        //default is 5 days a week, 8 hours per day
        $this->assertEquals(5, $this->ee->getDaysPerWeek());
        $this->assertEquals(8, $this->ee->getHoursPerDay());
        $this->assertEquals(40, $this->ee->getHoursPerWeek());
        $this->assertEquals(40, $this->ee->getWeeklyHours());
        $this->assertEquals(80, $this->ee->getBiweeklyHours());
        $this->assertEquals('86.6666', $this->ee->getSemimonthlyHours());
        $this->assertEquals(2080, $this->ee->getAnnualHours());
        $this->assertEquals(2080, $this->ee->getHoursPerYear());

        //reset ee because values are cached
        $this->ee = $this->getMockForAbstractClass('\BetaMFD\PayrollBundle\Model\Employee');
        $this->ee->setDaysPerWeek('2.35');
        $this->ee->setHoursPerDay('6.51');
        $this->assertEquals('2.35', $this->ee->getDaysPerWeek());
        $this->assertEquals('6.51', $this->ee->getHoursPerDay());
        $this->assertEquals('15.2985', $this->ee->getHoursPerWeek());
        $this->assertEquals('15.2985', $this->ee->getWeeklyHours());
        $this->assertEquals('30.5970', $this->ee->getBiweeklyHours());
        $this->assertEquals('33.1467', $this->ee->getSemimonthlyHours());
        $this->assertEquals('795.5220', $this->ee->getAnnualHours());
        $this->assertEquals('795.5220', $this->ee->getHoursPerYear());
    }

    public function testYearsHiredByDate()
    {
        $this->ee->setHireDate(new DateTime('2010-02-06'));

        $this->assertEquals(0.0027397260273972603,
            $this->ee->getYearsHiredByDate(new DateTime('2010-02-07')));

        $this->assertEquals(0.0054794520547945206,
            $this->ee->getYearsHiredByDate(new DateTime('2010-02-08')));

        $this->assertEquals(2.00273224043715846995,
            $this->ee->getYearsHiredByDate(new DateTime('2012-02-07')));

        $this->assertEquals(2,
            $this->ee->getYearsHiredByDate(new DateTime('2012-02-06')));

        $this->assertEquals(5,
            $this->ee->getYearsHiredByDate(new DateTime('2015-02-06')));

        $this->assertEquals(5.083333333333333333333,
            $this->ee->getYearsHiredByDate(new DateTime('2015-03-06')));

    }

}
