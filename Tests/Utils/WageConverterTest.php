<?php

namespace BetaMFD\PayrollBundle\Tests\Utils;

use BetaMFD\PayrollBundle\Utils\WageConverter;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class WageConverterTest extends TestCase
{
    const DECIMALS = 35;
    const HOURLY = '24.09674982389167157648350471153846153';
    const DAILY = '192.77399859113337261186803769230769230';
    const WEEKLY = '963.86999295566686305934018846153846153';
    const BIWEEKLY = '1927.73998591133372611868037692307692307';
    const SEMIMONTHLY = '2088.38498473727820329523707500000000000';
    const MONTHLY = '4176.76996947455640659047415000000000000';
    const ANNUALLY = '50121.23963369467687908568980000000000000';

    public function testAnnuals()
    {
        $monthly = WageConverter::annualToMonthly(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $monthly,
            'Expected ' . self::MONTHLY . " but got $monthly"
        );
        $semi = WageConverter::annualToSemimonthly(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $semi,
            'Expected ' . self::SEMIMONTHLY . " but got $semi"
        );
        $bi = WageConverter::annualToBiweekly(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $bi,
            'Expected ' . self::BIWEEKLY . " but got $bi"
        );
        $weekly = WageConverter::annualToWeekly(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $weekly,
            'Expected ' . self::WEEKLY . " but got $weekly");
        $day = WageConverter::annualToDaily(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $day,
            'Expected ' . self::DAILY . " but got $day");
        $hour = WageConverter::annualToHourly(self::ANNUALLY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testMonthlies()
    {
        $annual = WageConverter::monthlyToAnnual(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::monthlyToSemimonthly(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $semi,
            'Expected ' . self::SEMIMONTHLY . " but got $semi"
        );
        $bi = WageConverter::monthlyToBiweekly(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $bi,
            'Expected ' . self::BIWEEKLY . " but got $bi"
        );
        $weekly = WageConverter::monthlyToWeekly(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $weekly,
            'Expected ' . self::WEEKLY . " but got $weekly");
        $day = WageConverter::monthlyToDaily(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $day,
            'Expected ' . self::DAILY . " but got $day");
        $hour = WageConverter::monthlyToHourly(self::MONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testSemimonthlies()
    {
        $annual = WageConverter::semimonthlyToAnnual(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::semimonthlyToMonthly(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $semi,
            'Expected ' . self::MONTHLY . " but got $semi"
        );
        $bi = WageConverter::semimonthlyToBiweekly(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $bi,
            'Expected ' . self::BIWEEKLY . " but got $bi"
        );
        $weekly = WageConverter::semimonthlyToWeekly(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $weekly,
            'Expected ' . self::WEEKLY . " but got $weekly");
        $day = WageConverter::semimonthlyToDaily(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $day,
            'Expected ' . self::DAILY . " but got $day");
        $hour = WageConverter::semimonthlyToHourly(self::SEMIMONTHLY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testBiweeklies()
    {
        $annual = WageConverter::biweeklyToAnnual(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::biweeklyToMonthly(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $semi,
            'Expected ' . self::MONTHLY . " but got $semi"
        );
        $bi = WageConverter::biweeklyToSemimonthly(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $bi,
            'Expected ' . self::SEMIMONTHLY . " but got $bi"
        );
        $weekly = WageConverter::biweeklyToWeekly(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $weekly,
            'Expected ' . self::WEEKLY . " but got $weekly");
        $day = WageConverter::biweeklyToDaily(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $day,
            'Expected ' . self::DAILY . " but got $day");
        $hour = WageConverter::biweeklyToHourly(self::BIWEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testWeeklies()
    {
        $annual = WageConverter::weeklyToAnnual(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::weeklyToMonthly(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $semi,
            'Expected ' . self::MONTHLY . " but got $semi"
        );
        $bi = WageConverter::weeklyToSemimonthly(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $bi,
            'Expected ' . self::SEMIMONTHLY . " but got $bi"
        );
        $weekly = WageConverter::weeklyToBiweekly(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $weekly,
            'Expected ' . self::BIWEEKLY . " but got $weekly");
        $day = WageConverter::weeklyToDaily(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $day,
            'Expected ' . self::DAILY . " but got $day");
        $hour = WageConverter::weeklyToHourly(self::WEEKLY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testDaylies()
    {
        $annual = WageConverter::dailyToAnnual(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::dailyToMonthly(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $semi,
            'Expected ' . self::MONTHLY . " but got $semi"
        );
        $bi = WageConverter::dailyToSemimonthly(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $bi,
            'Expected ' . self::SEMIMONTHLY . " but got $bi"
        );
        $weekly = WageConverter::dailyToBiweekly(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $weekly,
            'Expected ' . self::BIWEEKLY . " but got $weekly");
        $day = WageConverter::dailyToWeekly(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $day,
            'Expected ' . self::WEEKLY . " but got $day");
        $hour = WageConverter::dailyToHourly(self::DAILY, self::DECIMALS);
        $this->assertEquals(
            self::HOURLY,
            $hour,
            'Expected ' . self::HOURLY . " but got $hour");
    }

    public function testHourlies()
    {
        $annual = WageConverter::hourlyToAnnual(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::ANNUALLY,
            $annual,
            'Expected ' . self::ANNUALLY . " but got $annual"
        );
        $semi = WageConverter::hourlyToMonthly(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::MONTHLY,
            $semi,
            'Expected ' . self::MONTHLY . " but got $semi"
        );
        $bi = WageConverter::hourlyToSemimonthly(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::SEMIMONTHLY,
            $bi,
            'Expected ' . self::SEMIMONTHLY . " but got $bi"
        );
        $weekly = WageConverter::hourlyToBiweekly(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::BIWEEKLY,
            $weekly,
            'Expected ' . self::BIWEEKLY . " but got $weekly");
        $day = WageConverter::hourlyToWeekly(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::WEEKLY,
            $day,
            'Expected ' . self::WEEKLY . " but got $day");
        $hour = WageConverter::hourlyToDaily(self::HOURLY, self::DECIMALS);
        $this->assertEquals(
            self::DAILY,
            $hour,
            'Expected ' . self::DAILY . " but got $hour");
    }


}
