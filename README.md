
Composer install directions deliberately left out. Will add in some day when this is ready to be used.

This is not meant to be a complete payroll system, but a jumping off point to building your own custom system. I'm working on two dramatically different payroll systems at the moment and I'm building this as the common ground between both of them so that I don't have to repeat so many concepts between the two.


### Enable the Bundle (Symfony 2.8)

Enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new BetaMFD\PayrollBundle\BetaMFDPayrollBundle(),
        );

        // ...
    }

    // ...
}
```

### Enable the Bundle In Symfony 3.4+

Enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:
```php
    BetaMFD\PayrollBundle\BetaMFDPayrollBundle::class => ['all' => true],
```




### Notes

This system is created using only entity Models so that you can pick and choose what you actually need and extend off them in your own PayrollBundle.



#### Add Employees

Create an Employee Entity and extend it from the model.
Create an ID field. It doesn't have to be a generated value but if it's not, you may want to write your own setter with any custom logic. A basic setter is in the model.

```
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\PayrollBundle\Model\Employee as BaseEmployee;

/**
 * @ORM\Table(name="employee")
 * @ORM\Entity
 */
class Employee extends BaseEmployee
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    //any other mappings you want

}

```

#### Add Pay Types
There are two Entity Models needed for Pay Types - PayType and PayTypeCategory. Category contains how the pay type affects the net (+, -, or null) and can contain other broad settings that always apply to a pay type. Suggested pay type categories include: deductions, employer, pay, taxes. One of my bundles has 7 different pay type categories spread over 37 different pay types, while my other bundle has only a few categories for only 18 pay types. Suggested starting  pay types include: regular, ot, vacation, holiday. You will also need pay types for federal taxes, any deductions you have and whatnot.

I personally like to have string for IDs for PayType and PayTypeCategory so I can see at a glance from the database what a pay type is, but you can use auto-incrementing IDs if you prefer. Just change the definition of the ID.

For each entity you need to create an ID field and extend off of the model. You will also need to set the PayTypeCategoryInterface up in your settings.


```
<?php
# PayTypeCategory

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\PayrollBundle\Model\PayTypeCategory as BaseCategory;

/**
 * PayTypeCategory
 *
 * @ORM\Table(name="pay_type_category")
 * @ORM\Entity
 */
class PayTypeCategory extends BaseCategory
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=4)
     * @ORM\Id
     */
    protected $id;

    //add any more category-wide settings and custom functions you need
}

```

```
<?php
# PayType

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\PayrollBundle\Model\PayType as BaseType;

/**
 * PayType
 *
 * @ORM\Table(name="pay_type")
 * @ORM\Entity()
 */
class PayType extends BaseType
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=12)
     * @ORM\Id
     */
    protected $id;

    //add any more category-wide settings and custom functions you need
}
```


```
# app/config/config.yml

doctrine:
    orm:
        resolve_target_entities:
            # Set this to wherever you actually create your PayTypeCategory Entity
            BetaMFD\PayrollBundle\Model\PayTypeCategoryInterface: AppBundle\Entity\PayTypeCategory
```


From there you can start adding custom logic. For example, I need to know
frequently whether a pay type is vacation or not, so I have a custom function in the PayType Entity:

```
    public function isVacation()
    {
        return $this->id === 'vacation';
    }
```

#### Holidays
The Holiday entity model assumes you know how to write php-parsable strings.

The real advantage of using this model is the extra functions that figure
out a date based on a given year. So if you have a date string "December 25"
it can give that back to you as a DateTime or a string using a provided year.

It also has settings for moving holidays to Friday/Monday if needed.
If you use getDateOff($year) it will move the day off to Friday or Monday appropriately and return that DateTime object for the given $year.

All functions that figure out dates will save the calculated value
so you can call it over and over again without recalculating it.

Similar to other Models, you just need to make an entity and extend it
off the model. I've found no need to make the ID strings, so my example below is just an auto-generated integer.

```
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BetaMFD\PayrollBundle\Model\Holiday as BaseHoliday;

/**
 * Holiday
 *
 * @ORM\Table(name="holiday")
 * @ORM\Entity()
 */
class Holiday extends BaseHoliday
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;
}
```
