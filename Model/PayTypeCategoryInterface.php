<?php

namespace BetaMFD\PayrollBundle\Model;

interface PayTypeCategoryInterface
{
    public function __toString();

    /**
     * Get id
     *
     * @return string
     */
    public function getId();

    /**
     * Set id
     *
     * @param string $id
     *
     * @return PayTypeCategories
     */
    public function setId($id);

    /**
     * Set category
     *
     * @param string $category
     *
     * @return PayTypeCategories
     */
    public function setCategory($category = null);

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory();

    /**
     * Set payDirection
     *
     * @param string $payDirection
     *
     * @return PayTypeCategories
     */
    public function setPayDirection($payDirection = null);

    /**
     * Get payDirection
     *
     * @return string
     */
    public function getPayDirection();

}
