<?php

namespace BetaMFD\PayrollBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

abstract class Employee
{
    const WEEKS_IN_A_YEAR = 52;

    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     */
    protected $department;

    /**
     * @var string
     *
     * @ORM\Column(name="first", type="string", length=50, nullable=false)
     */
    protected $first;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    protected $preferredFirst;

    /**
     * @var string
     *
     * @ORM\Column(name="last", type="string", length=50, nullable=false)
     */
    protected $last;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     */
    protected $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true)
     */
    protected $title;

    /**
     * @var string
     *
     * @ORM\Column(name="hourly_rate", type="decimal", precision=7, scale=4, nullable=true)
     */
    protected $hourlyRate;

    /**
     * @var string
     *
     * @ORM\Column(name="period_salary", type="decimal", precision=7, scale=2, nullable=true)
     */
    protected $periodSalary;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_salary", type="boolean", nullable=false)
     */
    protected $isSalary;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_hourly", type="boolean", nullable=false)
     */
    protected $isHourly;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_commission", type="boolean", nullable=false)
     */
    protected $isCommission;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_raise_date", type="date", nullable=true)
     */
    protected $lastRaiseDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="raise_entered_date", type="date", nullable=true)
     */
    protected $raiseEnteredDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="hire_date", type="date", nullable=true)
     */
    protected $hireDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="in_house", type="boolean", nullable=false)
     */
    protected $inHouse = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    protected $active = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="officer", type="boolean", nullable=false)
     */
    protected $officer = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="can_accrue_vacation", type="boolean", nullable=false)
     */
    protected $canAccrueVacation = true;

    /**
     * @var string
     *
     * @ORM\Column(name="vacation_accrual_rate", type="decimal", precision=6, scale=3, nullable=true)
     */
    protected $vacationAccrualRate;

    /**
     * @var string
     *
     * @ORM\Column(name="hours_per_day", type="decimal", precision=5, scale=2, nullable=true)
     */
    protected $hoursPerDay = '8.00';

    /**
     * @var string
     *
     * @ORM\Column(name="days_per_week", type="decimal", precision=5, scale=2, nullable=true)
     */
    protected $daysPerWeek = '5.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="full_time", type="boolean", nullable=false)
     */
    protected $fullTime = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="temp", type="boolean", nullable=false)
     */
    protected $temp = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $onLeave = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    protected $leaveReason;

    /**
     * @var string
     * @ORM\Column(type="decimal", precision=10, scale=4, nullable = true)
     */
    protected $vacationBalance;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    protected $phone2;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    protected $state;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $zip;

    /**
     * This is to prevent potential race conditions, especially w/ vac balance
     * @var integer
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    protected $version;

    /** @var string */
    protected $weeklyHours;

    /** @var string */
    protected $biweeklyHours;

    /** @var string */
    protected $semimonthlyHours;

    /** @var string */
    protected $annualHours;

    /**
     * Calculated field based on birth date
     * uses DateTime::diff
     * @var string
     */
    protected $age;

    /**
     * yearsHired
     * Calculated field based on hire date
     * @var string
     */
    protected $yearsHired;


    /**
     * @return string $first $last
     */
    public function __toString()
    {
        return $this->first . ' ' . $this->last;
    }

    /**
     * get the full name of the employee
     * @return string first last
     */
    public function getName()
    {
        return $this->first . ' ' . $this->last;
    }

    /**
     * Get the value of Preferred First or First
     *
     * @return string
     */
    public function getCommonFirst()
    {
        if (!empty($this->preferredFirst)) {
            return $this->preferredFirst;
        }
        return $this->first;
    }

    public function getCommonName()
    {
        return $this->getCommonFirst() . ' ' . $this->getLast();
    }

    /**
     * Get the first character of the last name
     * @return string
     */
    public function getLastInitial()
    {
        return $this->last[0];
    }

    /**
     * Get the first character of the first name
     * @return string
     */
    public function getFirstInitial()
    {
        return $this->first[0];
    }

    public function getInitials()
    {
        if (empty($this->first[0]) and empty($this->last[0])) {
            return;
        }
        return $this->first[0] . $this->last[0];
    }

    /**
     * Checks to see if the Employee is in-house
     * @return boolean
     */
    public function isInHouse()
    {
        return $this->inHouse;
    }

    public function isTemp()
    {
        return $this->temp;
    }

    /**
     * Checks to see if the Employee is active
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * Checks to see if the Employee is an officer
     * @return boolean
     */
    public function isOfficer()
    {
        return $this->officer;
    }

    /**
     * Checks to see if the Employee can accrue vacation
     * @return boolean
     */
    public function canAccrueVacation()
    {
        return $this->canAccrueVacation;
    }

    /**
     * Checks to see if the Employee is Hourly
     * @return boolean
     */
    public function isHourly()
    {
        return $this->isHourly;
    }

    /**
     * Checks to see if the Employee is Salary
     * @return boolean
     */
    public function isSalary()
    {
        return $this->isSalary;
    }

    /**
     * Checks to see if the Employee is full time
     * @return boolean
     */
    public function isFullTime()
    {
        return $this->fullTime;
    }

    /**
    * Checks to see if the Employee is part time
     * @return boolean
     */
    public function isPartTime()
    {
        return !$this->fullTime;
    }

    /**
     * Gets weekly hours based on hours per day and days per week
     * @return string
     */
    public function getWeeklyHours()
    {
        if (empty($this->weeklyHours)) {
            $this->weeklyHours = bcmul($this->hoursPerDay, $this->daysPerWeek, 4);
        }
        return $this->weeklyHours;
    }

    /**
     * Gets weekly hours based on hours per day and days per week
     * @return string
     */
    public function getHoursPerWeek()
    {
        return $this->getWeeklyHours();
    }

    /**
     * Gets biweekly hours based on hours per day and days per week
     * @return string
     */
    public function getBiweeklyHours()
    {
        if (empty($this->biweeklyHours)) {
            $this->biweeklyHours = bcmul($this->getWeeklyHours(), 2, 4);
        }
        return $this->biweeklyHours;
    }

    /**
     * Gets semimonthly hours based on hours per day and days per week
     * @return string
     */
    public function getSemimonthlyHours()
    {
        if (empty($this->semimonthlyHours)) {
            //weekly hours * 52 / 24
            $this->semimonthlyHours = bcdiv($this->getAnnualHours(), 24, 4);
        }
        return $this->semimonthlyHours;
    }

    /**
     * Gets annual hours based on hours per day and days per week
     * @return string
     */
    public function getAnnualHours()
    {
        if (empty($this->annualHours)) {
            $this->annualHours = bcmul($this->getWeeklyHours(), self::WEEKS_IN_A_YEAR, 4);
        }
        return $this->annualHours;
    }

    /**
     * Gets annual hours based on hours per day and days per week
     * @return string
     */
    public function getHoursPerYear()
    {
        return $this->getAnnualHours();
    }

    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of Department
     *
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set the value of Department
     *
     * @param string department
     *
     * @return self
     */
    public function setDepartment($department)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get the value of First
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set the value of First
     *
     * @param string first
     *
     * @return self
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * Get the value of Last
     *
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set the value of Last
     *
     * @param string last
     *
     * @return self
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get the value of Preferred First
     *
     * @return string
     */
    public function getPreferredFirst()
    {
        return $this->preferredFirst;
    }

    /**
     * Set the value of Preferred First
     *
     * @param string $preferredFirst
     *
     * @return self
     */
    public function setPreferredFirst($preferredFirst)
    {
        $this->preferredFirst = $preferredFirst;

        return $this;
    }

    /**
     * Get the value of Birth Date
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * Set the value of Birth Date
     *
     * @param \DateTime birthDate
     *
     * @return self
     */
    public function setBirthDate(\DateTime $birthDate = null)
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Get the age of the Employee based on their birth date
     * @return integer
     */
    public function getAge()
    {
        if (empty($this->age)) {
            $birthdate = $this->getBirthDate();
            if (empty($birthdate)) {
                return;
            } else {
                $this->age = $birthdate->diff(new DateTime('now'))->y;
            }
        }
        return $this->age;
    }

    /**
     * Get the value of Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of Title
     *
     * @param string title
     *
     * @return self
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of Hourly Rate
     *
     * @return string
     */
    public function getHourlyRate()
    {
        return $this->hourlyRate;
    }

    /**
     * Set the value of Hourly Rate
     *
     * @param string hourlyRate
     *
     * @return self
     */
    public function setHourlyRate($hourlyRate = null)
    {
        $this->hourlyRate = $hourlyRate;
        $this->setIsSalary(false);

        return $this;
    }

    /**
     * Get the value of Period Salary
     *
     * @return string
     */
    public function getPeriodSalary()
    {
        return $this->periodSalary;
    }

    /**
     * Set the value of Period Salary
     *
     * @param string periodSalary
     *
     * @return self
     */
    public function setPeriodSalary($periodSalary = null)
    {
        $this->periodSalary = $periodSalary;
        $this->setIsSalary(true);

        return $this;
    }

    /**
     * Get the value of Is Salary
     *
     * @return bool
     */
    public function getIsSalary()
    {
        return $this->isSalary;
    }

    /**
     * Set the value of Is Salary
     *
     * @param bool isSalary
     *
     * @return self
     */
    public function setIsSalary($isSalary)
    {
        $this->isSalary = $isSalary;
        $this->isHourly = !$isSalary;

        return $this;
    }

    /**
     * Get the value of Is Hourly
     *
     * @return bool
     */
    public function getIsHourly()
    {
        return $this->isHourly;
    }

    /**
     * Set the value of Is Hourly
     *
     * @param bool isHourly
     *
     * @return self
     */
    public function setIsHourly($isHourly)
    {
        $this->isHourly = $isHourly;
        $this->isSalary = !$isHourly;

        return $this;
    }

    /**
     * Get the value of Is Commission
     *
     * @return bool
     */
    public function getIsCommission()
    {
        return $this->isCommission;
    }

    /**
     * Get the value of Is Commission
     *
     * @return bool
     */
    public function isCommission()
    {
        return $this->isCommission;
    }

    /**
     * Get the value of Is Commission
     *
     * @return bool
     */
    public function isCommissionOnly()
    {
        return $this->isCommission and !$this->isHourly and !$this->isSalary;
    }


    /**
     * Set the value of Is Commission
     *
     * @param bool $isCommission
     *
     * @return self
     */
    public function setIsCommission($isCommission)
    {
        $this->isCommission = $isCommission;

        return $this;
    }

    /**
     * Get the value of Last Raise Date
     *
     * @return \DateTime
     */
    public function getLastRaiseDate()
    {
        return $this->lastRaiseDate;
    }

    /**
     * Set the value of Last Raise Date
     *
     * @param \DateTime lastRaiseDate
     *
     * @return self
     */
    public function setLastRaiseDate(\DateTime $lastRaiseDate = null)
    {
        $this->lastRaiseDate = $lastRaiseDate;

        return $this;
    }

    /**
     * Get the value of Hire Date
     *
     * @return \DateTime
     */
    public function getHireDate()
    {
        return $this->hireDate;
    }

    /**
     * Set the value of Hire Date
     *
     * @param \DateTime hireDate
     *
     * @return self
     */
    public function setHireDate(\DateTime $hireDate = null)
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    /**
     * Returns how many years an employee has been hired
     * based on hire date
     *
     * Please take into account that this uses float math and is not precise
     * but for most purposes it is good enough!
     *
     * @return float YearsHired
     */
    public function getYearsHired()
    {
        if (!empty($this->yearsHired)) {
            return $this->yearsHired;
        }
        if (empty($this->getHireDate())) {
            return;
        }
        $this->yearsHired = $this->getYearsHiredByDate(new DateTime());
        return $this->yearsHired;
    }

    /**
     * Returns how many years an employee has been hired
     * based on arbitrary date
     *
     * Please take into account that this uses float math and is not precise
     * but for most purposes it is good enough!
     *
     * @param  DateTime $date
     * @return float YearsHired
     */
    public function getYearsHiredByDate(\DateTime $date)
    {
        $interval = $this->getYearsHiredIntervalByDate($date);
        if (!$interval){
            return null;
        }
        // years = (years +  months/12 + days/365)
        $daysThisYear = $date->format('L') ? 366 : 365; //leap years
        $yr = $interval->format('%y%'); // years
        $mo = $interval->format('%m%') / 12; //months/12
        $days = $interval->format('%d%') / $daysThisYear; //days/365

        return $yr + $mo + $days;
    }

    /**
     * Returns a DateInterval object based on hire date
     *
     * Now you can do your own math
     * if getYearsHiredByDate isn't good enough for you.
     *
     * @param  DateTime $date
     * @return DateInterval Time Hired
     */
    public function getYearsHiredIntervalByDate(\DateTime $date = null)
    {
        $date_of_hire = $this->getHireDate();
        if (empty($date_of_hire)) {
            return false;
        }
        if (empty($date)) {
            $date = new DateTime();
        }
         return $date->diff($date_of_hire);
    }

    /**
     * Get the value of In House
     *
     * @return bool
     */
    public function getInHouse()
    {
        return $this->inHouse;
    }

    /**
     * Set the value of In House
     *
     * @param bool inHouse
     *
     * @return self
     */
    public function setInHouse($inHouse)
    {
        $this->inHouse = $inHouse;

        return $this;
    }

    /**
     * Get the value of Active
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set the value of Active
     *
     * @param bool active
     *
     * @return self
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get the value of Officer
     *
     * @return bool
     */
    public function getOfficer()
    {
        return $this->officer;
    }

    /**
     * Set the value of Officer
     *
     * @param bool officer
     *
     * @return self
     */
    public function setOfficer($officer)
    {
        $this->officer = $officer;

        return $this;
    }

    /**
     * Get the value of Can Accrue Vacation
     *
     * @return bool
     */
    public function getCanAccrueVacation()
    {
        return $this->canAccrueVacation;
    }

    /**
     * Set the value of Can Accrue Vacation
     * IF FALSE will set the vacation accrual rate to null!
     *
     * @param bool canAccrueVacation
     *
     * @return self
     */
    public function setCanAccrueVacation($canAccrueVacation)
    {
        $this->canAccrueVacation = $canAccrueVacation;
        if (!$canAccrueVacation) {
            $this->vacationAccrualRate = null;
        }

        return $this;
    }

    /**
     * Get the value of Vacation Accrual Rate
     *
     * @return string
     */
    public function getVacationAccrualRate()
    {
        return $this->vacationAccrualRate;
    }

    /**
     * Set the value of Vacation Accrual Rate
     *
     * IF NOT EMPTY will set canAccrueVacation to true!
     *
     * @param string vacationAccrualRate
     *
     * @return self
     */
    public function setVacationAccrualRate($vacationAccrualRate = null)
    {
        $this->vacationAccrualRate = $vacationAccrualRate;
        if (!empty($this->vacationAccrualRate)) {
            $this->canAccrueVacation = true;
        }

        return $this;
    }

    /**
     * Get the value of Hours Per Day
     *
     * @return string
     */
    public function getHoursPerDay()
    {
        return $this->hoursPerDay;
    }

    /**
     * Set the value of Hours Per Day
     *
     * @param string hoursPerDay
     *
     * @return self
     */
    public function setHoursPerDay($hoursPerDay = null)
    {
        $this->hoursPerDay = $hoursPerDay;

        return $this;
    }

    /**
     * Get the value of Days Per Week
     *
     * @return string
     */
    public function getDaysPerWeek()
    {
        return $this->daysPerWeek;
    }

    /**
     * Set the value of Days Per Week
     *
     * @param string daysPerWeek
     *
     * @return self
     */
    public function setDaysPerWeek($daysPerWeek = null)
    {
        $this->daysPerWeek = $daysPerWeek;

        return $this;
    }

    /**
     * Get the value of Full Time
     *
     * @return boolean
     */
    public function getFullTime()
    {
        return $this->fullTime;
    }

    /**
     * Set the value of Full Time
     *
     * @param boolean fullTime
     *
     * @return self
     */
    public function setFullTime($fullTime)
    {
        $this->fullTime = $fullTime;

        return $this;
    }

    /**
     * Get the value of Raise Entered Date
     *
     * @return \DateTime
     */
    public function getRaiseEnteredDate()
    {
        return $this->raiseEnteredDate;
    }

    /**
     * Set the value of Raise Entered Date
     *
     * @param \DateTime raiseEnteredDate
     *
     * @return self
     */
    public function setRaiseEnteredDate(\DateTime $raiseEnteredDate = null)
    {
        $this->raiseEnteredDate = $raiseEnteredDate;

        return $this;
    }

    /**
     * Get the value of Vacation Balance
     *
     * @return string
     */
    public function getVacationBalance()
    {
        return $this->vacationBalance;
    }

    /**
     * Add an amount to the vacation balance
     * @param string $vacation
     * @return string vacationBalance
     */
    public function addVacation($vacation)
    {
        $this->vacationBalance = round(bcadd($this->vacationBalance, $vacation, 6), 4);
        return $this->vacationBalance;
    }

    /**
     * Get the value of Temp
     *
     * @return boolean
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * Set the value of Temp
     *
     * @param boolean $temp
     *
     * @return self
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;

        return $this;
    }

    /**
     * Get the value of On Leave
     *
     * @return boolean
     */
    public function getOnLeave()
    {
        return $this->onLeave;
    }

    /**
     * Set the value of On Leave
     *
     * @param boolean $onLeave
     *
     * @return self
     */
    public function setOnLeave($onLeave)
    {
        $this->onLeave = $onLeave;
        if ($onLeave) {
            $this->active = false;
        } else {
            $this->active = true;
            $this->leaveReason = null;
        }

        return $this;
    }

    /**
     * Get the value of Leave Reason
     *
     * @return string
     */
    public function getLeaveReason()
    {
        return $this->leaveReason;
    }

    /**
     * Set the value of Leave Reason
     *
     * @param string $leaveReason
     *
     * @return self
     */
    public function setLeaveReason($leaveReason)
    {
        $this->leaveReason = $leaveReason;
        if (!empty($leaveReason)) {
            $this->setOnLeave(true);
        } else {
            $this->setOnLeave(false);
        }

        return $this;
    }

    /**
     * Get the value of Phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set the value of Phone
     *
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get the value of Phone
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set the value of Phone
     *
     * @param string $phone2
     *
     * @return self
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get the value of Address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set the value of Address
     *
     * @param string $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get the value of City
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set the value of City
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get the value of State
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of State
     *
     * @param string $state
     *
     * @return self
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get the value of Zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set the value of Zip
     *
     * @param string $zip
     *
     * @return self
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get the value of This is to prevent potential race conditions, especially w/ vac balance
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set the value of This is to prevent potential race conditions, especially w/ vac balance
     *
     * @param integer $version
     *
     * @return self
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }
}
