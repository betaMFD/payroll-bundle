<?php

namespace BetaMFD\PayrollBundle\Model;

use Doctrine\ORM\Mapping as ORM;


abstract class PayType
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=false)
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=50, nullable=true)
     */
    protected $description;

    /**
     * @var PayTypeCategory
     *
     * @ORM\ManyToOne(targetEntity="BetaMFD\PayrollBundle\Model\PayTypeCategoryInterface")
     */
    protected $cat;

    /**
     * @var boolean
     *
     * used for OT eligibility
     *
     * @ORM\Column(name="time_worked", type="boolean", nullable=false)
     */
    protected $timeWorked = false;

    public function __toString()
    {
        return $this->description;
    }

    /**
     * If time is actually worked, it's eligibile for OT
     * Example: Vacation is not eligibile for an OT calculation
     *
     * @return boolean whether or not this pay type is time worked
     */
    public function isOtEligible()
    {
        return $this->timeWorked;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param string id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get payDirection
     *
     * @return string
     */
    public function getPayDirection()
    {
        return $this->getCat()->getPayDirection();
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return PayTypes
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return PayTypes
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set cat
     *
     * @param \PayrollBundle\Entity\PayTypeCategories $cat
     *
     * @return PayTypes
     */
    public function setCat(\BetaMFD\PayrollBundle\Model\PayTypeCategoryInterface $cat = null)
    {
        $this->cat = $cat;

        return $this;
    }

    /**
     * Get cat
     *
     * @return \BetaMFD\PayrollBundle\Model\PayTypeCategoryInterface
     */
    public function getCat()
    {
        return $this->cat;
    }

    /**
     * Gets direction of pay (up/down/0/null) based on the category
     *
     * @return string payDirection
     */
    public function getDirection()
    {
        if (is_object($this->cat)) {
            return $this->cat->getPayDirection();
        }
    }


    /**
     * Get the value of Time Worked
     *
     * @return boolean
     */
    public function getTimeWorked()
    {
        return $this->timeWorked;
    }

    /**
     * Set the value of Time Worked
     *
     * @param boolean timeWorked
     *
     * @return self
     */
    public function setTimeWorked($timeWorked)
    {
        $this->timeWorked = $timeWorked;

        return $this;
    }

}
