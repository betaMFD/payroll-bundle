<?php

namespace BetaMFD\PayrollBundle\Model;

use Doctrine\ORM\Mapping as ORM;

class GLCode
{
    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=4)
     * @ORM\Id
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    protected $description;


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Code
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Code
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
