<?php

namespace BetaMFD\PayrollBundle\Model;

use Doctrine\ORM\Mapping as ORM;

abstract class Holiday
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="holiday", type="string", length=200, nullable=false)
     */
    protected $holiday;

    /**
     * @var string
     *
     * PHP parsable string
     *
     * @ORM\Column(name="string", type="string", length=200, nullable=false)
     */
    protected $string;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer", length=4, nullable=true)
     */
    protected $year;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    protected $weekendMove;

    const WEEKEND_MOVE_TO_MONDAY = 'Mon';
    const WEEKEND_MOVE_TO_FRIDAY = 'Fri';

    /**
     * Cached date-string, array keyed on [year]
     * @var array
     */
    protected $dateString = [];

    /**
     * Cached DateTime, array keyed on [year]
     * @var array
     */
    protected $date = [];

    /**
     * Cached DateTime off, array keyed on [year]
     * @var array
     */
    protected $dateOff = [];

    /**
     * Is the given date a holiday?
     * 
     * @param  DateTime $date
     * @return boolean is the given date this holiday or not
     */
    public function isHoliday(\DateTime $date)
    {
        $holiday = $this->getDate($date->format('Y'));
        return $holiday->format('Ymd') === $date->format('Ymd');
    }

    /**
     * Gets the actuall day off for the Holiday
     * If the date lands on a weekend it moves it to Friday or Monday appropriately
     * Caches the value in case you need it more than once.
     *
     * @param  int $year
     * @return DateTime $dayOff
     */
    public function getDateOff($year)
    {
        if (empty($year)) {
            throw new Exception('No year provided. You must provide a year to get the date off.');
        }

        if (!empty($this->dateOff[$year])) {
            return $this->dateOff[$year];
        }
        if (empty($this->date[$year])) {
            $this->getDateString($year);
        }
        if (!empty($this->getYear() and $year != $this->getYear())) {
            //nope
            return;
        }
        $this->dateOff[$year] = clone $this->date[$year];
        $w = $this->date[$year]->format('w');
        //6 is Saturday
        //0 is Sunday
        if ($w > 0 and $w < 6) {
            return $this->dateOff[$year];
        }
        $d = $this->dateOff[$year]; //object by reference
        $move = $this->weekendMove;

        //Saturday to Friday
        if ($w == 6 and ($move == 'Fri' or empty($move))) {
            $d->modify('-1 day');
            return $d;
        }

        //Sunday to Monday
        if ($w == 0 and ($move == 'Mon' or empty($move))) {
            $d->modify('+1 day');
            return $d;
        }

        //Saturday to Monday
        if ($w == 6 and $move == 'Mon') {
            $d->modify('+2 days');
            return $d;
        }

        //Sunday to Friday
        if ($w == 0 and $move == 'Fri') {
            $d->modify('-2 days');
            return $d;
        }
    }

    /**
     * Generates a string 'Y-m-d' for a the day off in a given $year
     * Caches the value in case you need it more than once
     *
     * $year IS IGNORED IF HOLIDAY ALREADY HAS A SET YEAR
     * Example: if you give a 2020 year but the year
     *   is set in the Entity as 2018, you will get a string for 2018
     *
     * @param  int $year
     * @return string 'Y-m-d' formatted date string
     */
    public function getDateString($year = null)
    {
        if (empty($year) && empty($this->getYear())) {
            throw new \Exception('Holiday has no year.');
        }

        if (!empty($this->dateString[$year])) {
            return $this->dateString[$year];
        }
        $holidayYear = $this->getYear();
        $string = $this->getString();

        if (!empty($holidayYear)) {
            $date = date('Y-m-d', strtotime($string));
        } else {
            $date = date('Y-m-d', strtotime($string . ' ' . $year));
        }
        $this->dateString[$year] = $date;
        $this->date[$year] = new \DateTime($date);
        return $date;
    }


    /**
     * Get the value of Date
     *
     * $year IS IGNORED IF HOLIDAY ALREADY HAS A SET YEAR
     * Example: if you give a 2020 year but the year
     *   is set in the Entity as 2018, you will get a result for 2018
     *
     * @param  int $year
     * @return DateTime
     */
    public function getDate($year)
    {
        $this->getDateString($year);
        return $this->date[$year];
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set holiday
     *
     * @param string $holiday
     *
     * @return self
     */
    public function setHoliday($holiday)
    {
        $this->holiday = $holiday;

        return $this;
    }

    /**
     * Get holiday
     *
     * @return string
     */
    public function getHoliday()
    {
        return $this->holiday;
    }

    /**
     * Set string
     *
     * @param string $string PHP-parsable $string that the code will turn into a DateTime
     * @return self
     */
    public function setString($string)
    {
        $this->string = $string;

        return $this;
    }

    /**
     * Get string
     *
     * @return string
     */
    public function getString()
    {
        return $this->string;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return self
     */
    public function setYear($year)
    {
        $this->year = $year;

        if (!empty($year)) {
            $this->getDateString($year);
        }

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Get the value of Weekend Move
     *
     * @return string
     */
    public function getWeekendMove()
    {
        return $this->weekendMove;
    }

    /**
     * Set the value of Weekend Move
     *
     * @param string weekendMove ('Mon' or 'Fri')
     *
     * @return self
     */
    public function setWeekendMove($weekendMove = null)
    {
        if ($weekendMove === self::WEEKEND_MOVE_TO_MONDAY
            or $weekendMove === self::WEEKEND_MOVE_TO_FRIDAY
            or $weekendMove === null
        ) {
            $this->weekendMove = $weekendMove;
        } else {
            throw new \Exception('The only accepted values are '
                . self::WEEKEND_MOVE_TO_MONDAY
                . 'and'
                . self::WEEKEND_MOVE_TO_FRIDAY
                . '. If you need a different value, create your own setWeekendMove() method.');
        }

        return $this;
    }

}
