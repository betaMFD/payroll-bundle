<?php

namespace BetaMFD\PayrollBundle\Model;

use Doctrine\ORM\Mapping as ORM;


abstract class PayTypeCategory implements PayTypeCategoryInterface
{
    /**
     * @var mixed
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=50, nullable=true)
     */
    protected $category;

    /**
     * @var string
     *
     * This should only ever be set to +, -, 0, or null
     *
     * @ORM\Column(name="pay_direction", type="string", length=1, nullable=true)
     */
    protected $payDirection;

    public function __toString()
    {
        return $this->category;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param string $id
     *
     * @return PayTypeCategories
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return PayTypeCategories
     */
    public function setCategory($category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set payDirection
     * This should only ever be set to +, -, 0, or null
     *
     * @param string $payDirection
     *
     * @return PayTypeCategories
     */
    public function setPayDirection($payDirection = null)
    {
        if ($payDirection === '+'
            or $payDirection === '-'
            or $payDirection === null
            or $payDirection === '0'
            or $payDirection === 0
        ) {
            $this->payDirection = $payDirection;
        } else {
            throw new \Exception('Pay Direction can only be +, -, 0, or null.
              If you need it to be something different you will need to
              write your own setPayDirection() function.');
        }

        return $this;
    }

    /**
     * Get payDirection
     *
     * @return string
     */
    public function getPayDirection()
    {
        return $this->payDirection;
    }

}
